<?php

namespace Blok\Shortcode\Tests;

use Blok\Shortcode\Facades\Shortcode;
use Blok\Shortcode\ServiceProvider;
use Orchestra\Testbench\TestCase;

class ShortcodeTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'shortcode' => Shortcode::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
