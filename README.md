# Shortcode

[![Build Status](https://travis-ci.org/blok/shortcode.svg?branch=master)](https://travis-ci.org/blok/shortcode)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/blok/shortcode/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/blok/shortcode/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/blok/shortcode/badge.svg?branch=master)](https://coveralls.io/github/blok/shortcode?branch=master)

[![Packagist](https://img.shields.io/packagist/v/blok/shortcode.svg)](https://packagist.org/packages/blok/shortcode)
[![Packagist](https://poser.pugx.org/blok/shortcode/d/total.svg)](https://packagist.org/packages/blok/shortcode)
[![Packagist](https://img.shields.io/packagist/l/blok/shortcode.svg)](https://packagist.org/packages/blok/shortcode)

Package description: CHANGE ME

## Installation

Install via composer
```bash
composer require blok/shortcode
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Blok\Shortcode\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Blok\Shortcode\Facades\Shortcode::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\Shortcode\ServiceProvider" --tag="config"
```

## Usage

How to register a shortcode ?



## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [](https://github.com/blok/shortcode)
- [All contributors](https://github.com/blok/shortcode/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
