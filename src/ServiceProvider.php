<?php

namespace Blok\Shortcode;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        require_once __DIR__ . '/helpers.php';
    }

    public function register()
    {
        $this->app->singleton('shortcode', function () {
            return new Shortcode();
        });
    }
}
