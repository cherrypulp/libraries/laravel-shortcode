<?php

use Blok\Utils\Arr;
use Blok\Utils\Utils;

if (!function_exists('\shortcode')) {

    function shortcode($string, $data = [], $params = ['nl2br' => false, 'delimiters' => ['{', '}']])
    {
        Arr::mergeWithDefaultParams($params);

        if ($params['nl2br']) {
            $string = nl2br($string);
        }

        $data = array_dot($data);

        $string = Utils::smrtr($string, $data, $params['delimiters']);

        $string = app('shortcode')->compile($string);

        return $string;
    }

}

if (!function_exists('\s')) {
    function s($string, $data = [], $params = ['nl2br' => false, 'delimiters' => ['{', '}']])
    {
        return shortcode($string, $data, $params);
    }
}
